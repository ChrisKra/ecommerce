<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use Illuminate\Http\Request;

class AnnouncementController extends Controller
{

    public function createAnnouncement() { 
        return view ('announcements.create');
    }

    public function showAnnouncement(Announcement $announcement) {
        $announcements = Announcement::where('is_accepted', true)->take(4)->get();
        return view('announcements.show', compact('announcement', 'announcements'));
    }

    public function indexAnnouncements()  {
        $announcements = Announcement::where('is_accepted', true)->paginate(12);
        return view ('announcements.index', compact('announcements'));
    }

    public function showByFirst() {
        $announcements = Announcement::where('is_accepted', true)->orderBy('created_at', 'DESC')->paginate(12);
        return view('announcements.index', compact('announcements'));
    }

    public function priceDesc() {
        $announcements = Announcement::where('is_accepted', true)->orderBy('price', 'DESC')->paginate(12);
        return view('announcements.index', compact('announcements'));
    }

    public function priceAsc() {
        $announcements = Announcement::where('is_accepted', true)->orderBy('price', 'ASC')->paginate(12);
        return view('announcements.index', compact('announcements'));
    }
    
}
