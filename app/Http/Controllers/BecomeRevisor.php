<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Traits\Serialization;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BecomeRevisor extends Mailable
{




    public $user;
    public function __construct(User $user)
    {
            $this->user = $user;
    }



    public function build(){
    return $this->from('presto.it@noreply.com')->view('mail.become_revisor');

    }



}
