<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category;
use App\Models\Announcement;
use App\Models\Image;
use Illuminate\Support\Facades\DB;

class AdsSearchBar extends Component
{
    public $query;
    public $announcements;
    public $selectIndex;
    public $categories;
    public $images;

    public function mount() {
        $this->resetQuery();
    }

    //Funzione per escapa 
    public function resetQuery() {
        $this->query = '';
        $this->announcements = [];
        $this->categories = [];
        $this->images = [];
        $this->selectIndex = 0;
    }

    public function incrementSelectIndex() {
        // se arrivo in fondo alla lista e clicco ancora in basso
        // allora torno in alto
        if ($this->selectIndex === count($this->announcements) - 1) {
            $this->selectIndex = 0;
            return;
        }
        $this->selectIndex++;
    }

    public function decrementSelectIndex() {
        // se arrivo in fondo alla lista e clicco ancora in basso
        // allora torno in alto
        if ($this->selectIndex === 0) {
            $this->selectIndex = count($this->announcements) - 1;
            return;
        }
        $this->selectIndex--;
    }

    public function selectAds()
    {
        $announcements = $this->announcements[$this->selectIndex] ?? null;
        if ($announcements) {
            $this->redirect(route('announcement.show', $announcements['id']));
        }
    }

    public function updatedQuery() {
        sleep(1); // Attendi 1 secondo e poi fai partire la funzione
        $this->announcements = Announcement::where('title', 'like', '%' . $this->query . '%')->where('is_accepted', true)->get()->toArray();
        $this->categories = Category::all();
        // dd($this->images);
        // $this->announcements = DB::table('announcements')
        //     ->leftjoin('categories', 'categories.id', '=', 'announcements.category_id')->get();
    }


    public function render()
    {
        return view('livewire.ads-search-bar');
    }
}
