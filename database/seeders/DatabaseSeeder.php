<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Category;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
    * Seed the application's database.
    *
    * @return void
    */
    public function run()
    {
        $categories = [
            [
                'name'=>'Motori',
                'icon'=>'/frontend/motori.png'
            ],
            [
                'name'=>'Informatica',
                'icon'=>'/frontend/informatica.png'
            ],
            [
                'name'=>'Elettrodomestici',
                'icon'=>'/frontend/elettrodomestici.png'
            ],
            [
                'name'=>'Libri',
                'icon'=>'/frontend/libri.png'
            ],
            [
                'name'=>'Giochi',
                'icon'=>'/frontend/giochi.png'
            ],
            [
                'name'=>'Sport',
                'icon'=>'/frontend/sport.png'
            ],
            [
                'name'=>'Immobili',
                'icon'=>'/frontend/immobili.png'
            ],
            [
                'name'=>'Telefoni',
                'icon'=>'/frontend/telefoni.png'
            ],
            [
                'name'=>'Arredamento',
                'icon'=>'/frontend/arredamento.png'
            ],
        ];


        foreach($categories as $category){
            DB::table('categories')->insert([
                'name'=>$category['name'],
                'icon'=>$category['icon'],
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
            ]);
        }
    }
}
