<?php
return [

    'welcome1'=>'Willkommen bei',
    'welcome2'=>'einer Plattform zum Online-Kaufen und -Verkaufen',

    'slogan1'=>'Folgen Sie Ihrer Intuition, tun Sie, wie wir gehen Nase...',
    'slogan2'=>'was warten Sie jetzt auf!',

    'insurance1'=>'Schauen Sie sich unsere Versicherung',
    'insurance2'=>'Unterstützung 24 Stunden am Tag',
    'insurance3'=>'eine sichere Website, die Sie im Falle von Betrug erstattet',

    'categoryDesk'=>'Durchsuchen Sie Anzeigen nach Kategorie',

    'arrivals'=>'Neu eingetroffen',

    'placeholderSearch'=>'Suchen Sie hier nach Ihren Anzeigen',

    'price'=>'Preis',

    'view'=>'Blick',

    'publish'=>'Veröffentlicht am:',

    'work1'=>'Arbeit mit',
    'work2'=>'UNS',
    'work3'=>'Wir suchen den',
    'work4'=>'Post Auditor ',
    'work5'=>'in unserem Team, möchten Sie mit uns arbeiten?',
    'work6'=>'Werden Sie ein Auditor',
    
    'founder1'=>'Founder',
    'founder2'=>'Ich bin der Gründer von early, arbeite gerne mit meinen Kollegen und verbringe Zeit mit ihnen im und außerhalb des Büros',
    'founder3'=>'Art Director',
    'founder4'=>'Ich habe immer zu viele Ideen für neue Gruppenprojekte, ich hoffe, sie alle zu verwirklichen, aber die Zeit ist nie genug. Küsse und Umarmungen',
    'founder5'=> 'CIO',
    'founder6'=>'Der Sport ist der Schlüssel für mich, es ist ein bisschen wie das Leben eine endlose Reihe von Fans, wo die Grundlagen Spaß haben und gewinnen sind',
    'founder7'=> 'CMO',
    'founder8'=> 'Die beste Strategie ist immer ein breites Lächeln, es ist der einfachste und effektivste Weg, um effizient zu kommunizieren. Viele Grüße',
    'manager1'=>'CTO',
    'manager2'=>'Ich nenne mich Giramondo, ich programmiere smart für Presto , am Ende einer Reise gehe ich immer dorthin zurück, wo mein Herz mich nach Rome führt',

    'Home'=>'Startseite',
    'ads'=>'Add',
    'access'=>'Login',
    'ad_ads'=>'Kleinanzeigen',
    'admin'=>'Bereich Auditor',
    'out'=>'Logout',

    'Home_mob'=>'STARTSEITE',
    'ads_mob'=>'ANZEIGEN',
    'search_mob'=>'SUCHE',
    'account_mob'=>'KONTO',
    'add_mob'=>'ADD',
    'profile_mob'=>'PROFIL',
    'out_mob'=>'LOGOUT',

    // headerMobile

    'payoff'=>'Du hast eine Nase fürs Geschäft',

    //search.bar

    'no_result'=>'kein Ergebnis',

    //create.announcement

    'create1'=>'Erstellen Sie Ihre eigene Anzeige',
    'create2'=>'Ankündigungstitel',
    'create3'=>'Beschreibung:',
    'create4'=>'Preis:',
    'create5'=>'Option',
    'create6'=>'Kategorie auswählen',
    'create7'=>'Produktbild:',
    'create8'=>'Vorschaubild',
    'create9'=>'Löschen',
    'create10'=>'Erstellen',

    // become.revisor

    'become1'=>'Ein Benutzer hat darum gebeten, mit uns zu arbeiten',
    'become2'=>'Hier sind seine Daten',
    'become3'=>'Name',
    'become4'=>'Email',
    'become5'=>'Wenn Sie es zu einem Auditor machen möchten, klicken Sie hier',
    'become6'=>'Revidieren',
    
    //index.revisor

    'user'=>'Aus:',
    'accept'=>'Akzeptieren',
    'decline'=>'Verweigern',

    'explore'=>'Entdecken Sie die Kategorie',
    'no_ads'=>'Keine Anzeigen in dieser Kategorie',
    'no_ads1'=>'Publican:',
    'no_ads2'=>'Neue Anzeige',
    'author'=>'Author:',

   
    'filter'=> 'Ihre Suche filtern',
    'filter1'=> 'Filtert',
    'ordin'=>'Befiehlt',
    'decreasing'=> 'Preis absteigend',
    'increasing' => 'Preis steigend',
    'last'=> 'Letzte einträge',
    'our'=> 'Unsere anzeigen',
    'change'=> 'Es gibt keine Anzeigen für diese Suche. Versuchen Sie sich zu ändern',

    'ad'=>'Ankündigung',
    'back'=> 'Zurück',
    'category'=> 'Kategorie',

    'mail'=>'Email Adresse',
    'password'=>'Password',
    'no_account'=>'Wenn Sie kein Konto haben',
    'no_account1'=>'Registriertes',
    'no_account2'=>'Von hier',

    'username'=>'Benutzername',
    'repeat'=> 'Passwort wiederholen',

    
    

    

];
