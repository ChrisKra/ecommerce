<?php
return [
    // welcome.blade
    'welcome1'=>'Benvenuto su',
    'welcome2'=>'una piattaforma per comprare e vendere on line',

    'slogan1'=>'Segui il tuo intuito,fai come noi vai a naso...',
    'slogan2'=>'che aspetti inizia ora!',

    'insurance1'=>'Scopri la nostra assicurazione',
    'insurance2'=>'un assistenza 24 ore su 24!',
    'insurance3'=>'un sito sicuro, che ti rimborsa in caso di truffa',

    'categoryDesk'=>'Naviga tra gli annunci filtrando per categoria',

    'arrivals'=>'Ultimi arrivi',

    'placeholderSearch'=>'Cerca i tuoi annunci qui',

    'price'=>'Prezzo',

    'view'=>'Visualizza',

    'publish'=>'Pubblicato il:',

    'work1'=>'Lavora con',
    'work2'=>'NOI',
    'work3'=>'cerchiamo la figura di',
    'work4'=>'revisore',
    'work5'=>'di annunci nel nostro Team, vuoi lavorare con noi?',
    'work6'=>'Diventa un Revisore',

    'founder1'=>'Founder',
    'founder2'=>'Sono il fondatore di Presto, adoro lavorare con i miei colleghi e passare il tempo con loro dentro e fuori l\'ufficio',
    'founder3'=> 'Art Director',
    'founder4'=> 'Ho sempre troppe idee per nuovi progetti di gruppo spero di realizzarli tutti ma il tempo non è mai abbastanza',
    'founder5'=> 'CIO',
    'founder6'=> 'Lo sport è fondamentale per me, è come la vita una serie di patite dove gli elementi fondamentali sono divertirsi e vincere',
    'founder7'=> 'CMO',
    'founder8'=> 'La strategia migliore è sempre sfoggiare un grande sorriso, è il modo più semplice e efficace una comunicazione efficiente',
    'manager1'=>'CTO',
    'manager2'=>'Sono un Giramondo faccio smart programming, alla fine di un viaggio torno sempre dove mi porta il cuore, Roma',

    
    'ads'=>'Annunci',
    'access'=>'Accedi',
    'ad_ads'=>'Inserisci Annuncio',
    'admin'=>'Zona revisore',
    'out'=>'Logout',

     // headerMobile

    'Home_mob'=>'HOME',
    'ads_mob'=>'ANNUNCI',
    'search_mob'=>'CERCA',
    'account_mob'=>'ACCOUNT',
    'add_mob'=>'ADD',
    'profile_mob'=>'PROFILO',
    'out_mob'=>'ESCI',


    'payoff'=>'hai fiuto per gli affari',

    //search.bar

    'no_result'=>'Nessun risultato',

     //create.announcement

    'create1'=>'Crea il tuo annuncio',
    'create2'=>'Titolo annuncio',
    'create3'=>'Descrizione:',
    'create4'=>'Prezzo:',
    'create5'=>'Categorie',
    'create6'=>'Scegli la Categoria',
    'create7'=>'Immagini Prodotto :',
    'create8'=>'Anteprima Immagini',
    'create9'=>'Cancella',
    'create10'=>'Crea',

    // become.revisor

    'become1'=>'Un utente ha richiesto di lavorare con noi',
    'become2'=>'Ecco i suoi dati:',
    'become3'=>'Nome:',
    'become4'=>'Email:',
    'become5'=>'Se vuoi renderlo revisore clicca qui :',
    'become6'=>'Rendi revisore',

    //index.revisor

    'user'=>'Di:',
    'accept'=>'Accetta',
    'decline'=>'Rifiuta',
    'revisorend'=>'Complimenti non hai annunci di revisionare! Ottimo lavoro 💪',

    //category.show

    'explore'=>'Esplora la categoria',
    'no_ads'=>'Non sono presenti annunci per questa categoria',
    'no_ads1'=>'Pubblicane uno:',
    'no_ads2'=>'Nuovo annuncio',
    'author'=>'Autore:',

    // index.announcement

    'filter'=> 'Filtra la ricerca',
    'filter1'=> 'Filtra',
    'ordin'=>'Ordina',
    'decreasing'=> 'Prezzo descrescente',
    'increasing' => 'Prezzo crescente',
    'last'=> 'Ultimi inseriti',
    'our'=> 'I nostri annunci',
    'change'=> 'Non ci sono annunci per questa ricerca. Prova a cambiare',
    
     // show.announcement

    'ad'=>'Annuncio',
    'back'=> 'Torna indietro',
    'category'=> 'Categoria',

    // login

    'mail'=>'Indirizzo Mail',
    'password'=>'Password',
    'no_account'=>'Se non hai un account',
    'no_account1'=>'Registrati',
    'no_account2'=>'da qui',

    // register

    'username'=>'Username',
    'repeat'=> 'Ripeti password',




];
 