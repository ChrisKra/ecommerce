{{-- <div class="container-fluid">
    <footer class="py-5 ">
     
    

      <div class="d-flex flex-column flex-sm-row justify-content-between py-4 my-4 border-top">
        <p>© 2022 Presto, Inc. All rights reserved.</p>
        <ul class="list-unstyled d-flex">
         
        </ul>
      </div>
  </footer>
</div>


 --}}

 <div class="container-fluid bg-dark text-warning mt-5">
  <div class="row justify-content-center pt-4">
      <div class="col-9">
          <footer class="py-5">
              <div class="row">
                  <div class="col-12 d-flex">
                      <img class="img-fluid me-4 mb-5" src="" alt="">
                      <h5 class="fs-1 fw-bold text-maincolor">Presto.it</h5>
                  </div>
              </div>
              <!-- colonne link -->
              <div class="row align-items-center bg-dark">
                <div class="col-6 col-md-4 mb-3">
                  <h4 class="my-3 text-maincolor ms-3">Contatti</h4>
                  <ul class="nav mx-3 d-flex justify-content-start flex-column my-3">
                    <li class="nav-item mb-2"><a href="#" class="p-0 nav-link text-white"><i class="bi bi-telephone-fill fs-3 pe-1"></i>+39 0801122333</a></li>
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white"><i class="bi bi-phone-fill fs-3 pe-1"></i>+39 328 4455666</a></li>
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white"><i class="bi bi-envelope-fill pe-1 fs-3"></i>mannaggetta51@presto.it</a></li>
                   
                  </ul>                       
                </div>
        
                
                 
                
        
                <div class="col-6 col-md-3 mb-3 d-flex justify-content-center pe-5">
                <img src="/frontend/loghettoSofisticatobiancoTavoladisegno1.png" alt="" style="height: 150px">
                </div>
        
                <div class="col-md-5 offset-md-1 m-0">
                  <form>
                    <h4 class="text-center text-white">ISCRIVITI ALLA NEWSLETTER</h4>
                    <p class="text-center text-white">Ogni giorno nuovi e interessanti annunci <br> potrebbero passarti sotto al naso!</p>
                    <div class="d-flex flex-column flex-sm-row w-100 my-0">
                      <label for="newsletter1" class="visually-hidden ">Email address</label>
                      <input id="newsletter1" type="text" class="form-control  border-dark rounded-0 bg-mainlight m-0 text-center" placeholder="Email address">
                      <button class="btn btn-primary border-dark bg-maincolor rounded-0 fs-button text-dark m-0" type="button">Subscribe</button>
                    </div>
                  </form>
                 
                  <ul class="nav mx-3 d-flex justify-content-center mt-5">
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white ps-2"><i class="bi bi-facebook fs-3 mx-2"></i></a></li>
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white ps-2"><i class="bi bi-tiktok fs-3 mx-2"></i></a></li>
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white ps-2"><i class="bi bi-instagram fs-3 mx-2"></i></a></li>
                   
                  </ul>
                </div>
              </div>
        
              
              <div class="d-flex flex-column flex-sm-row justify-content-between py-4 my-4 border-top text-white">
                  <p>© 2022 Mannaggetta, Inc. All rights reserved.</p>
              
              </div>
          </footer>
      </div>
  </div>
</div>
