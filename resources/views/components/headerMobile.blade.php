<div class="container-fluid bg-light sticky-top d-block d-sm-none">
    <div class="row">
        <div class="col-3 m-auto px-4 text-center">
            <button class="btn m-0 p-0" type="button" data-bs-toggle="offcanvas" data-bs-target="#staticBackdrop" aria-controls="staticBackdrop">
                <span><i class="bi bi-list fs-1 text-dark p-2"></i></span>
            </button>
        </div>

        <div class="col-6 pt-3 my-auto p-0 text-center">
            <img class="img-fluid presto" src="/frontend/logoprestosofisticato.png" alt="">
            <h5 class="text-center fw-bold" id="subtitleMob">{{__('ui.payoff')}}</h5>
        </div>

        <div class="col-3 m-auto px-4 text-center">
            <button class="btn m-0 p-0" type="button">
                <span><i class="bi bi-bag-fill fs-1 text-dark p-2"></i></span>
            </button>
        </div>

        {{-- Live Search Livewire --}}
        <div class="container sticky" id="searchZoneInHeadMob">
            <div class="row">
                <div class="col-12 px-0">
                        @livewire('ads-search-bar')
                </div>
            </div>
        </div>
        {{-- Fine Live Search Livewire --}}
    </div>
    
</div>
    
    <div class="offcanvas offcanvas-start bg-offcanvas" data-bs-backdrop="static" tabindex="-1" id="staticBackdrop" aria-labelledby="staticBackdropLabel">
        <div class="offcanvas-header bg-offcanvas">
            <h5 class="offcanvas-title" id="staticBackdropLabel"></h5>
            <button type="button" class="btn-close btn-close-white me-2 mt-2" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>

        <div class="offcanvas-body bg-offcanvas text-center pb-2">
            <div class="row">
            <h4 class="text-white">Seleziona Lingua</h4>
            <div class="col-4 border border-white py-2 px-0">
                
                    <x-_locale lang='it' nation='it'/>
               
            </div>
            <div class="col-4 border border-white py-2 px-0">
                
                    <x-_locale lang='en' nation='en'/>
                
            </div>          
            <div class="col-4 border border-white py-2 px-0">
                
                    <x-_locale lang='de' nation='de'/>
                
            </div>
            </div>
            <div class="vh-75"></div>
            @guest 

            @else 
            <div class="col-2 px-0 m-0 text-center pt-1 nav-mob-costum my-2 w-100 border border-white btn_rosso">
                <a class="nav-link" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    <i class="bi bi-box-arrow-right fs-3 text-white"></i>
                    <p class="fs-button text-white">{{__('ui.out_mob')}}</p>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </div>
            @endguest
        </div>
    </div>