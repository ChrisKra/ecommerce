<nav class="navbar navbar-expand-lg bg-maincolor d-block d-sm-none fixed-bottom p-0 pb-2">
    <div class="container-fluid p-0 m-0">
        <div class="row justify-content-evenly w-100  m-0 align-items-center ">

            <div class="col-2 px-0 m-0 text-center pt-1 nav-mob-costum my-2">
                <a href="{{route('welcome')}}" class="nav-link">
                    <i class="bi bi-house fs-3"></i>
                    <p class="fs-button position-relative" id="homeButtonMob">{{__('ui.Home_mob')}}</p>
                </a>
            </div>

            <div class="col-2 px-0 m-0 text-center pt-1 nav-mob-costum my-2">
                <a href="{{route('announcements.index')}}" class="nav-link">
                    <i class="bi bi-chat-left-dots fs-3"></i>
                    <p class="fs-button position-relative" id="adsButtonMob">{{__('ui.ads_mob')}}</p>
                </a>
            </div>

            @guest
            <div class="col-2 px-0 m-0 text-center pt-1 nav-mob-costum my-2">
                <a href="#search" class="nav-link">
                    <i class="bi bi-search fs-3"></i>
                     <p class="fs-button position-relative">{{__('ui.search_mob')}}</p>
                </a>
            </div>

            <div class="col-2 px-0 m-0 text-center pt-1  nav-mob-costum my-2">
                <a href="{{route('login')}}" class="nav-link">
                    <i class="bi bi-person-square fs-3"></i>
                    <p class="fs-button position-relative" id="accountButtonMob">{{__('ui.account_mob')}}</p>
                </a>
            </div>

            @else
            <div class="col-2 px-0 m-0 text-center pt-1 nav-mob-costum my-2">
                <a href="{{route('announcements.create')}}" class="nav-link">
                    <i class="bi bi-plus-square fs-3 status position-relative"></i>
                    <p class="fs-button position-relative" id="addButtonMob">{{__('ui.add_mob')}}</p>
                </a>
            </div>

            <div class="col-2 px-0 m-0 text-center pt-1 nav-mob-costum my-2">
                <a href="" class="nav-link">
                    <i class="bi bi-person-circle fs-3"></i>
                    <p class="fs-button position-relative">{{__('ui.profile_mob')}}</p>
                </a>
            </div>

            {{-- Button revisor --}}
            @if (Auth::User()->is_revisor)
            <div class="col-2 px-0 m-0 text-center pt-1 nav-mob-costum my-2">
                <a href="{{ route('revisor.index') }}" class="nav-link">
                    <i class="bi bi-person-check fs-3"></i>                    <p class="fs-button position-relative">{{__('ui.admin')}}</p>
                    <span class="position-absolute top-0 px-1 end-0 bg-danger rounded-circle text-white">
                        {{ App\Models\Announcement::toBeRevisionedCount() }}
                        <span class="visually-hidden">unread messages</span>
                    </span>
                </a>
            </div>


            {{-- <li class="nav-item">
                <a class="nav-link active position-relative me-2 pe-4 revisor"
                    href="{{ route('revisor.index') }}">{{__('ui.admin')}}
                    <span class="position-absolute top-0 px-1 end-0 bg-danger rounded-circle text-white">
                        {{ App\Models\Announcement::toBeRevisionedCount() }}
                        <span class="visually-hidden">unread messages</span>
                    </span>
                </a>
            </li> --}}
        @endif
           
            @endguest

        </div>



    </div>
  </nav>
