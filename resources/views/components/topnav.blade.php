<nav class="navbar navbar-expand-lg bg-dark d-none d-sm-block z-9">
    <div class="container">
        {{-- dark mode --}}
        <i class="bi bi-sun-fill text-white me-2"></i>
        <div class="form-check form-switch">
            <input class="form-check-input px-4" type="checkbox" id="checkbox">
            <label class="form-check-label" for="checkbox"></label>
        </div>
        <i class="bi bi-moon-fill text-white ms-2"></i>
        {{-- dark mode --}}

        {{-- Scelta lingua --}}
        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
            <li class="nav-item">
                <x-_locale lang='it' nation='it'/>
            </li>

            <li class="nav-item">
                <x-_locale lang='en' nation='en'/>
            </li>
            
            <li class="nav-item">
                <x-_locale lang='de' nation='de'/>
            </li>
        </ul>
      
      </div>
    </div>
  </nav>