<div>
    <div class="container- fluid">
        <div class="row justify-content-center justify-content-md-between align-items-center">
            <div class="col-md-5 col-12 order-2 order-md-2">
            <img src="/frontend/creaAnnuncio.png" alt="" class="img-fluid" style="height: 667px">
            </div>
            <div class="col-11 col-md-6 p-md-5 p-4 mx-md-1 my-md-5 mb-2 bg-mainlight shadow-box order-1 order-md-1 mt-3">
                <h1 class="my-3 text-center fs-2" id="titlePage">{{__('ui.create1')}}</h1>
                
                <form wire:submit.prevent="store" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="title" class="form-label">{{__('ui.create2')}}</label>
                        <input wire:model="title" type="text" class="form-control border border-dark rounded-0 @error('title') is-invalid @enderror">
                        @error('title') {{$message}} @enderror
                    </div>

                    <div class="mb-3">
                        <label for="body" class="form-label">{{__('ui.create3')}}</label>
                        <textarea wire:model="body" type="text" class="form-control border border-dark rounded-0 @error('body') is-invalid @enderror"></textarea>
                        @error('body') {{$message}} @enderror
                    </div>

                    <div class="mb-3">
                        <label for="price" class="form-label">{{__('ui.create4')}}</label>
                        <input wire:model="price" type="number" class="form-control border border-dark rounded-0 @error('price') is-invalid @enderror">
                        @error('price') {{$message}} @enderror
                    </div>


                    <div class="mb-3">
                        <label for="category">{{__('ui.create5')}}</label>
                        <select wire:model.defer="category" class="form-control border border-dark rounded-0 @error('category') is-invalid @enderror" id="category">
                            <option value="">{{__('ui.create6')}}</option>
                            @foreach ($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        @error('category') {{$message}} @enderror
                    </div>

                    {{-- Input immagini --}}
                    <div class="mb-3">
                        <label class="form-label">{{__('ui.create7')}}</label>
                        <input wire:model='temporary_images' name="images" multiple class="form-control form-control-sm 
                        border border-dark rounded-0 @error('temporary_images.*') is-invalid @enderror" type="file">
                        @error('temporary_images.*') {{$message}} @enderror
                    </div>

                    {{-- preview immagini importate --}}
                    @if(!empty($images))
                    <div class="row">
                        <div class="col-12">
                            <p>{{__('ui.create8')}}</p>
                            <div class="row border border-info py-4">
                                @foreach($images as $key => $image)
                                    <div class="col my-3">
                                        <div class="img-preview mx-auto p-5" style="background-size: cover; background-position: center; background-image: url({{$image->temporaryUrl()}});"></div>
                                        <button type="button" class="btn btn-danger d-block text-center mt-3 mx-auto" wire:click='removeImage({{$key}})'>{{__('ui.create9')}}</button>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endif

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <button type="submit" class="btn mt-4 w-100 btn_giallo">{{__('ui.create10')}}</button>

                </form>

            </div>
        </div>
    </div>
</div>
