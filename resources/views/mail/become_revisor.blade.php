<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>

    <style>

        .titolo {
            font-family: 'Sora', sans-serif;
            color: #62B273;
            font-weight: 600;
        }

        .text-content {
            font-family: 'Montserrat', sans-serif;
            color: #000;
            font-size: 15px;
        }

        .bg-panna {
            background-color: #F5ECE9; 
        }

        .img1 {
            height: 150px;
        }

        .btn_verde {
            color: black;
            border: 1px solid black;
            border-radius: 0px;
            background-color: #62B273;
            font-family: 'Sora', sans-serif;
            font-weight: 900;
            transition: .4s;
            text-decoration: none !important;
            padding: 10px;
            margin-top: 20%;
        }

        .btn_verde:hover {
            color: black;
            border: 1px solid black;
            border-radius: 0px;
            background-color: #62B273;
            font-family: 'Sora', sans-serif;
            font-weight: 900;
            box-shadow: 5px 5px 0px 1px black;
            text-decoration: none !important;
            padding: 10px;
        }

        .btn_verde:active {
            border: 1px solid black;
            background-color: #62B273;
            box-shadow: 0px 0px 0px 1px #000;
            text-decoration: none !important;
            padding: 10px;
        }

        .text-center {
            text-align: center;
        }

        .shadow-box {
            border: 1px solid black;
            box-shadow: 5px 5px 0px 1px #000;
            border-radius: 0px;
        }
    </style>

  </head>
  <body>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <!-- logo -->
                <img class="img1 my-4" src="http://127.0.0.1:8000/frontend/Operasenzatitolo.gif" alt="gif">
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-center shadow-box bg-panna">
                <!-- logo -->
                <h1 class="titolo">{{ __('ui.become1') }}</h1>
                <h2 class="text-content">{{ __('ui.become2') }}</h2>
                <p class="text-content">{{ __('ui.become3') }} {{ $user->name }}</p>
                <p class="text-content">{{ __('ui.become4') }} {{ $user->email }}</p>
                <p class="text-content">{{ __('ui.become5') }} </p>
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-center">
                
                <a class="btn_verde" href="{{ route('make.revisor', compact('user')) }}">{{ __('ui.become6') }}</a>
                
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-center mt-5">
                <!-- logo -->
                <img class="" src="http://127.0.0.1:8000/frontend/logoprestosofisticato.png" alt="">
            </div>
        </div>
    </div>




    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
  </body>
</html>