<x-layout>
    <div class="min-vh-100">
        <p class="d-none" id="titlePage">revisore</p>


    <div class="container mt-5">
        <h1 class="textInDark">Annunci da revisionare:</h1>
        @if  ($announcement_to_check)
        <div class="row">
            {{-- titolo e descrizione --}}
            <div class="col-md-10">
                <h2 class="display-5 ms-3">{{$announcement_to_check->title}}</h2>
                <h4 class=" ms-3">{{__('ui.user')}} {{ $announcement_to_check->user->name ?? '' }}</h4>
                <h4 class="ms-3 pseudoBar position-relative">{{__('ui.publish')}} {{ $announcement_to_check->created_at->format('d/m/Y') }}</h4>
                <h4 class="ms-3"><strong>{{__('ui.create3')}}</strong> {{ $announcement_to_check->body }}</h4>
                <h4 class="ms-3 fs-5"><strong>{{__('ui.create4')}}</strong> € {{ $announcement_to_check->price }}</h4>
            </div>
            <div class="col-md-2 mt-5">
                <form method="POST" action="{{route('revisor.accept_announcement', ['announcement'=>$announcement_to_check])}}">
                    @csrf
                    @method('PATCH')
                    <button type="submit" class="btn btn_verde mb-3 w-100">{{__('ui.accept')}}</button>
                </form>
                <form method="POST" action="{{route('revisor.reject_announcement', ['announcement'=>$announcement_to_check])}}">
                    @csrf
                    @method('PATCH')
                    <button type="submit" class="btn btn_rosso w-100">{{__('ui.decline')}}</button>
            </div>
        </div>




        <div class="row mt-3">
            @if ($announcement_to_check->images)
            @foreach ($announcement_to_check->images as $image)
            <div class="col-3 col-revisor">
                <img src="{{Storage::url($image->path)}}" class="img-fluid p-3" alt="">
                <hr>
                <h5 class="fw-bold">Tags</h5>
                @foreach ($image->labels as $label) 
                    <p class="d-inline">{{$label}},</p>
                @endforeach
                
                <hr>
                
                <h5 class="fw-bold mt-3">Contenuti</h5>                            
            
                <p>Adulti : <i class="{{$image->adult}}"></i></p>
                <p>Satira : <span class="{{$image->spoof}}"></span></p>
                <p>Medicina : <span class="{{$image->medical}}"></span></p>
                <p>Violenza : <span class="{{$image->violence}}"></span></p>
                <p>Contenuto Ammiccante : <span class="{{$image->racy}}"></span></p>                  
           </div>              
            @endforeach
            @endif       

        </div>
        @else
        <div class='col-12'>
            <div class='alert alert-info mt-5 py-3 shadow-box'>
                <p class='fs-5 text-center'>{{__('ui.revisorend')}}</p>
            </div>
        </div>
        @endif
    </div>

    </div>
</x-layout>