<x-layout>
    <x-slot name='title'>Presto.it | Home </x-slot>
    <main>
        <p class="d-none" id="titlePage">Homepage</p>

        {{-- allert messages --}}
        <div class="container my-3">
            @if (session('access.denied'))
            <div class='alert alert-danger shadow-box'>
                {{ session('access.denied') }}
            </div>
            @endif

            @if (session('message'))
            <div class='fs-5 alert alert-success shadow-box'>
                {{ session('message') }}
            </div>
            @endif
        </div>

        <div class="container d-none d-sm-block">
            <div class="row my-4">
                <div class="col-md-4 d-flex align-items-center justify-content-center">
                    <img src="/frontend/Rainbow.png" alt="" style="height: 130px" class="anima_raimbow">
                </div>
                <div class="col-md-4 text-center">
                    <img src="/frontend/Operasenzatitolo.gif" alt="" style="height: 150px">
                    {{-- <h2 class="text-center text-maincolor">PRESTO</h2> --}}
                    <h5 class="text-center textInDark">hai fiuto per gli affari</h5>
                </div>
                <div class="col-md-4 d-flex align-items-center justify-content-center">
                    <img src="/frontend/Paperplane.png" alt="" style="height: 130px" class="anima_raimbow">
                </div>
            </div>
        </div>

        {{-- Boxs Home --}}
        <div class="container">
            <div class="row my-3 justify-content-center">
                <div class="col-12 col-md-3 box_ini order-1 order-md-1 p-0 m-0 d-flex align- ">
                    <img class="img-fluid m-0 p-0" src="/frontend/total-fiore.png" alt="">
                </div>


                <div class="col-12 col-md-3 box_ini p-2 order-2 order-md-2 bg-thirdlight">
                    <h3>{{ __('ui.welcome1') }} <span class="presto">PRESTO</span><br> {{ __('ui.welcome2') }}
                    </h3>
                    <p class=""> {{ __('ui.slogan1') }} <br><br> {{ __('ui.slogan2') }}</p>
                </div>


                <div class="col-12 col-md-3 box_ini p-2 order-4 order-md-4 bg-thirdlight">
                    <h3> {{ __('ui.insurance1') }} <span class="prestissimo">PRESTISSIMO</span></h3>
                    <p> {{ __('ui.insurance2') }}<br> <br> {{ __('ui.insurance3') }}</p>
                </div>

                <div class="col-12 col-md-3 box_ini order-3 order-md-3 p-0 m-0">
                    <img class="img-fluid m-0 p-0" src="/frontend/looking-totale.png" alt="">
                </div>
            </div>
        </div>

        {{-- Carosello categorie --}}
        <h3 class="text-center mt-5 fs-md-3 textInDark"> {{ __('ui.categoryDesk') }}</h3>
        <div class="container p-0">
            <div class="row slider">
                @foreach ($categories as $category)
                <div class="col-12 col-md-2">
                    <div class="bg-category p-md-4 rounded-0 mx-md-2">
                        <a href="{{ route('categoryShow', compact('category')) }}" class="nav-link text-center ">
                            <img src="{{ $category->icon }}"
                            class="card-img-top img-fluid p-md-0" alt="...">
                            
                            
                            {{-- <h4 class="d-none d-sm-block textInDark">{{ $category->name }}</h4> --}}
                        
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
            {{-- Fine carosello categorie --}}

            {{-- Ultimi 4 annunci --}}
            <div class="container bord bacheca">
                <div class="row">
                    <div class="col-12 col-md-3">
                        <h4 class="border border-dark radious-0 bg-third py-2 text-center">{{ __('ui.arrivals') }}</h4>
                    </div>

                    <div class="row ps-2 ms-1">
                        @foreach ($announcements as $announcement)
                        {{-- @dd($announcement) --}}
                        <div class="col-12 col-md-3 postit">
                            <div class="">
                                <img src="{{ !$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(660, 440): 'https://picsum.photos/400' }}"
                                class="card-img-top in_postit" alt="...">

                                <div class="p-0">
                                    <h4 class="card-title ">{{ $announcement->title }}</h4>
                                    <p class="card-text">{{ $announcement->body }}</p>
                                    <h4 class="card-text"> {{ __('ui.price') }}: € {{ $announcement->price }}</h4>
                                    <a href="{{ route('announcement.show', compact('announcement')) }}"
                                    class="btn btn_verde_postit">{{ __('ui.view') }}</a>

                                    {{-- <a href="#" class="btn btn-primary">Categoria:{{$announcement->category->name}}</a> --}}

                                    <p class="card-footer mt-2 mb-0 text-center text-md-end">{{ __('ui.publish') }}
                                        {{ $announcement->created_at->format('d/m/Y') }}
                                    </p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>



                </div>


            </div>
        </div>


        {{-- lavora con noi --}}
        <div class="container border border-dark radious-0 bg-mainlight my-5">
            <div class="row">
                <div class="col-4 col-md-3 p-0">
                    <h4 class="border border-dark radious-0 border-top-0 border-start-0 bg-maincolor py-2 text-center">Team</h4>
                </div>
            </div>

            <div class="row mb-2 slider_team">
                {{-- card diventa un revisore --}}
                <div class="col-11 col-md-2 bg-panna border border-dark radious-0 p-0 mx-md-2 my-3">
                    <div class="card position-relative">
                        <img src="frontend/lavoraConNoi.gif" alt="" class="img-size_team">
                        <img src="frontend/diventarevisoresfondo.png" class="card-img-top radious-0 border-bottom border-dark" alt="...">
                        <div class="card-body bg-third radious-0 ">
                            <h4> {{ __('ui.work1') }} <span class="prestissimo"> {{ __('ui.work2') }} </span> </h4>
                            <p class="">{{ __('ui.work3') }}<strong>{{ __('ui.work4') }}</strong>
                                {{ __('ui.work5') }}</p>
                                <div class="card-footer bg-third text-center pb-0">
                                    <a href="{{ route('become.revisor') }}" class="btn btn_verde">{{ __('ui.work6') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-11 col-md-2 bg-panna border border-dark radious-0 p-0 mx-md-2 my-3">
                        <div class="card position-relative">
                            <img src="frontend/giuseppe.png" alt="" class="img-size_team">
                            <img src="frontend/giuseppesfondo.png" class="card-img-top radious-0 border-bottom border-dark" alt="...">
                            {{-- cards 2 team --}}
                            <div class="card-body bg-panna radious-0 border">
                                <h4 class="card-title">{{ __('ui.founder1') }}</h4>
                                <p class="card-text">{{ __('ui.founder2') }}</p>
                                <div class="text-center mt-4">
                                    Giuseppe C.
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-11 col-md-2 bg-panna border border-dark radious-0 p-0 mx-md-2 my-3">
                        <div class="card position-relative">
                            <img src="frontend/christian.png" alt="" class="img-size_team">
                            <img src="frontend/chrisfondo.png" class="card-img-top radious-0 border-bottom border-dark" alt="...">
                            {{-- cards 2 team --}}
                            <div class="card-body bg-panna radious-0 ">
                                <h4 class="card-title">{{ __('ui.manager1') }}</h4>
                                <p class="card-text">{{ __('ui.manager2') }}</p>
                                <div class="text-center mt-4">
                                    Christian A. K.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-11 col-md-2 bg-panna border border-dark radious-0 p-0 mx-md-2 my-3">
                        <div class="card position-relative">
                            <img src="frontend/ceci.png" alt="" class="img-size_team">
                            <img src="frontend/cecisfondo.png" class="card-img-top radious-0 border-bottom border-dark" alt="...">
                            {{-- cards 2 team --}}
                            <div class="card-body bg-panna radious-0 ">
                                <h4 class="card-title">{{ __('ui.founder3') }}</h4>
                                <p class="card-text">{{ __('ui.founder4') }}</p>
                                <div class="text-center mt-4">
                                    Cecilia D. L.
                                </div>
                            </div>
                        </div>
                    </div> <div class="col-11 col-md-2 bg-panna border border-dark radious-0 p-0 mx-md-2 my-3">
                        <div class="card position-relative">
                            <img src="frontend/thomas.png" alt="" class="img-size_team">
                            <img src="frontend/thomassfondo.png" class="card-img-top radious-0 border-bottom border-dark" alt="...">
                            {{-- cards 2 team --}}
                            <div class="card-body bg-panna radious-0 ">
                                <h4 class="card-title">{{ __('ui.founder5') }}</h4>
                                <p class="card-text">{{ __('ui.founder6') }}</p>
                                <div class="text-center mt-4">
                                    Thomas P.
                                </div>
                            </div>
                        </div>
                    </div> <div class="col-11 col-md-2 bg-panna border border-dark radious-0 p-0 mx-md-2 my-3">
                        <div class="card position-relative">
                            <img src="frontend/michele.png" alt="" class="img-size_team">
                            <img src="frontend/michesfondo.png" class="card-img-top radious-0 border-bottom border-dark" alt="...">
                            {{-- cards 2 team --}}
                            <div class="card-body bg-panna radious-0">
                                <h4 class="card-title">{{ __('ui.founder7') }}</h4>
                                <p class="card-text">{{ __('ui.founder8') }}</p>
                                <div class="text-center mt-4">
                                    Michele D. L.
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </main>
    </x-layout>








