<?php

use App\Models\Announcement;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\AnnouncementController;
use App\Http\Controllers\RevisorController;

Route::controller(PublicController::class)->group(function () {
    Route::get('/' , 'welcome')->name('welcome');
    Route::get('/categoria/{category}','categoryShow')->name('categoryShow');
    Route::get('/ricerca/annuncio', 'searchAnnouncements')->name('announcements.search');
    //Cambio lingua
    Route::post('/lingua/{lang}', 'setLanguage')->name('set_language_locale');

});

// Rotte per gli annunci
Route::controller(AnnouncementController::class)->group(function () {
    Route::get('/nuovo/annuncio', 'createAnnouncement')->middleware('auth')->name('announcements.create');
    Route::get('/dettaglio/annuncio/{announcement}', 'showAnnouncement')->middleware('auth')->name('announcement.show');
    Route::get('/annunci/index', 'indexAnnouncements')->name('announcements.index');
    Route::get('/annunci/first', 'showByFirst')->name('showByFirst');
    Route::get('/annunci/desc', 'priceDesc')->name('priceDesc');
    Route::get('/annunci/asc', 'priceAsc')->name('priceAsc');



});

// Rotte per revisioni
Route::controller(RevisorController::class)->group(function(){
    Route::get('/richiesta/revisore', 'becomeRevisor')->middleware('auth')->name('become.revisor');
    Route::get('/revisor/home', 'index')->middleware('isRevisor')->name('revisor.index');
    Route::patch('/accetta/annuncio/{announcement}', 'acceptAnnouncement')->middleware('isRevisor')->name('revisor.accept_announcement');
    Route::patch('/rifiuta/annuncio/{announcement}', 'rejectAnnouncement')->middleware('isRevisor')->name('revisor.reject_announcement');

    Route::get('/rendi/revisore/{user}', 'makeRevisor')->name('make.revisor');
});





